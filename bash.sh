#!/bin/bash
sourcefiles='/tmp/test/files/'
archive='/tmp/test/archive/'
logfile='/tmp/test/script-log/sync.log'
file=$(find /tmp/test/files/ -type f -newermt "7 days ago" -printf '%T@ %p\n' | sort -n | tail -1 | cut -f2- -d" ")


echo "------Info об этом файле------" >> $logfile
echo $file | xargs stat >> /tmp/test/script-log/sync.log

echo "начало архивирования `date +"%Y-%m-%d_%H-%M-%S"`" >> $logfile
echo $file | xargs -I {} sh -c 'tar -czPf /tmp/test/files/$(date +%F-%H_%M_%S)_$(basename {} .log).tar.gz {}'

echo "------перемещение архива `date +"%Y-%m-%d_%H-%M-%S"`" >> $logfile
if find /tmp/test/files/ -iname "*.gz" -newermt "7 days ago" -type f -printf '%p\n' -exec mv {} //tmp/test/archive/ \; ; then
	rm $file
fi
