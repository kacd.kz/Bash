Существуют 3 директорий по пути /tmp/test/   
создаем их посредством mkdir  

1 - /tmp/test/files/  
2 - /tmp/test/archive/  
3 - .tmp/test/script-log/  

В директорий /files/ созданы несколько тестовых файлов  

___________________________________  
Теперь в баш скрипт.  Необходимо его добавить в кронтаб на выполнение каждые 15 минут.  
*/15 * * * * /tmp/test/bash.sh > /tmp/myjob.log 2>&1  
#!/bin/bash  
Объявляем переменные, которые можно использовать в скрипте.   


  
sourcefiles='/tmp/test/files/'  
archive='/tmp/test/archive/'  
logfile='/tmp/test/script-log/sync.log'  

file=$(find /tmp/test/files/ -type f -newermt "7 days ago" -printf '%T@ %p\n' | sort -n | tail -1 | cut -f2- -d" ")       
результат вывода команды вводим в переменную $file. С помощью Find находим файлы не старше 7 дней, сортируем и выбираем последний модифицированный из списка, результатом будет абсолютный путь к файлу.  

последний найденный файл полученный через переменную $file | выводим статистику  и отправляем данные в лог файл. Можно урезать лишнее добавив ключи -c"%y %s %n" к команде stat  
echo "Info об этом файле" >> $logfile        # отправляем в лог файл текст.  
echo $file | xargs stat  >> /tmp/test/script-log/sync.log      
#   


Поиск последних файлов и передача вывода на архивирование. Можно сделать условие, которое отправит в лог text архивирование завершено посредством if then.  
echo "начало архивирования `date +"%Y-%m-%d_%H-%M-%S"`" >> $logfile   # отправляем в лог текст.  
echo $file | xargs -I {} sh -c 'tar -czPf /tmp/test/files/$(date +%F-%H_%M_%S)_$(basename {} .log).tar.gz {}'   
#  



Поиск последних файлов c разрешением *gz и перемещение в другую директорию архива , если выполнено успешно then удалить исходный файл.
echo "------перемещение архива `date +"%Y-%m-%d_%H-%M-%S"`" >> $logfile  
if find /tmp/test/files/ -iname "*.gz" -newermt "7 days ago" -type f -printf '%p\n' -exec mv {} //tmp/test/archive/ \; ; then  
	rm $file                   
fi  
#  







